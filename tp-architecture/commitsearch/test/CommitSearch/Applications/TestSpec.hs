{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module CommitSearch.Applications.TestSpec where

import Data.Text (Text, isInfixOf)
import Data.Time.Clock.POSIX
import Effectful
import Effectful.Dispatch.Dynamic
import Effectful.Reader.Static
import Effectful.Writer.Dynamic
import Test.Hspec

import CommitSearch.Domain.Commit 
import CommitSearch.Effects.DbCommit
import CommitSearch.Applications.Test 

-------------------------------------------------------------------------------
-- testing data
-------------------------------------------------------------------------------

commit1, commit2 :: Commit
commit1 = MkCommit "id1" (posixSecondsToUTCTime 0) "title 1" "toto"
commit2 = MkCommit "id2" (read "2011-11-19 18:28:52.607875 UTC") "title game 2" "john doe"

commits :: [Commit]
commits = [commit1, commit2]

-------------------------------------------------------------------------------
-- interpreters
-------------------------------------------------------------------------------

runDbCommitReader :: Reader [Commit] :> es => Eff (DbCommit : es) a -> Eff es a
runDbCommitReader = interpret $ \_env -> \case
  AllCommits -> ask
  CommitsFromTitle txt -> filter (isInfixOf txt . title) <$> ask

-- TODO implémenter un interpréteur pour Logger via un Writer [Text]
-- runLoggerWriter

-------------------------------------------------------------------------------
-- runApps
-------------------------------------------------------------------------------

runApp1 :: Eff [DbCommit, Reader [Commit]] a -> a
runApp1 = runPureEff . runReader commits . runDbCommitReader

-- TODO runApp2

-------------------------------------------------------------------------------
-- tests
-------------------------------------------------------------------------------

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Applications Test" $ do

    it "runApp1" $ do
      let res = runApp1 testApp1
      res `shouldBe` (commits, [commit2])

    -- TODO
    -- it "runApp2" $ do
    --   let (res, logs) = runApp2 testApp2
    --   res `shouldBe` (commits, [commit2])
    --   logs `shouldBe` 
    --     [ "fetching all commits..."
    --     , "fetching commits containing 'game'..." ]

