
import CommitSearch.Applications.Test

main :: IO ()
main = do

  (commits1, commits2) <- runApp testApp2

  putStrLn "\ncommits1:"
  mapM_ print commits1

  putStrLn "\ncommits2:"
  mapM_ print commits2

