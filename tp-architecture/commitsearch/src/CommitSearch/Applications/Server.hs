{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module CommitSearch.Applications.Server where

import Control.Monad
import Control.Monad.Trans.Class
import Data.Text
import Data.Time.Format.ISO8601
import Effectful 
import Lucid
import Web.Scotty.Trans

import CommitSearch.Domain.Commit
import CommitSearch.Effects.DbCommit
import CommitSearch.Interpreters.DbCommitHttp

runServer :: Int -> IO ()
runServer port = scottyT port runApp serverApp

type AppM = Eff [DbCommit, IOE]

runApp :: AppM a -> IO a
runApp = runEff . db
  where
    db = runDbCommitHttp ulcoGitlab

serverApp :: ScottyT AppM ()
serverApp = do

  get "/api" $ do
    -- TODO afficher : "/api"
    commits <- lift allCommits
    json commits

  get "/" $ do
    patt <- queryParam "pattern" `rescue` (\(StatusError _ _) -> pure "")
    -- TODO afficher : "/ pattern=" <> patt
    commits <- lift $ commitsFromTitle patt
    html $ renderText $ mkPage commits patt

mkPage :: [Commit] -> Text -> Html ()
mkPage commits patt = doctypehtml_ $ do

    head_ $ do
      meta_ [charset_ "utf-8"]
      meta_ [ name_ "viewport"
            , content_ "width=device-width,initial-scale=1,shrink-to-fit=no"]
      title_ "CommitSearch"

    body_ $ do
      h1_ "CommitSearch"

      div_ $ form_ [action_ "/", method_ "get"] $ do
        input_ [name_ "pattern", value_ patt]
        input_ [type_ "submit"]

      div_ $ ul_ $ forM_ commits $ \commit -> li_ $ do
        toHtml $ iso8601Show $ created_at commit
        ": "
        strong_ $ toHtml $ title commit
        " ("
        toHtml $ author_name commit
        ")"

