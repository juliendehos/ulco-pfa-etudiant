{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module CommitSearch.Applications.Test where

import Effectful 

import CommitSearch.Domain.Commit 
import CommitSearch.Effects.DbCommit
import CommitSearch.Interpreters.DbCommitHttp

testApp1 :: (DbCommit :> es) => Eff es ([Commit], [Commit])
testApp1 = do
  res1 <- allCommits
  res2 <- commitsFromTitle "game"
  pure (res1, res2)

-- TODO
testApp2 :: (DbCommit :> es) => Eff es ([Commit], [Commit])
testApp2 = testApp1
-- testApp2 = do
--   TODO afficher : "fetching all commits..."
--   res1 <- allCommits
--   TODO afficher : "fetching commits containing 'game'..."
--   res2 <- commitsFromTitle "game"
--  pure (res1, res2)

runApp :: Eff [DbCommit, IOE] a -> IO a
runApp app = runEff $ db app
  where
    db = runDbCommitHttp ulcoGitlab

