
module CommitSearch.Domain.Commit where

import Data.Aeson
import Data.Text
import Data.Time
import GHC.Generics

data Commit = MkCommit
  { short_id :: Text
  , created_at :: UTCTime
  , title :: Text
  , author_name :: Text
  } deriving (Eq, Generic, Show)

instance FromJSON Commit

instance ToJSON Commit where
    toEncoding = genericToEncoding defaultOptions

