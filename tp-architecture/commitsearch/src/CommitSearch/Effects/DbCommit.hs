{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module CommitSearch.Effects.DbCommit where 

import Data.Text
import Effectful
import Effectful.TH

import CommitSearch.Domain.Commit

data DbCommit :: Effect where
  AllCommits :: DbCommit m [Commit]
  CommitsFromTitle :: Text -> DbCommit m [Commit]

makeEffect ''DbCommit

