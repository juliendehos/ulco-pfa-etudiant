{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module CommitSearch.Interpreters.DbCommitHttp where

import Data.Aeson (eitherDecode)
import Data.Either (fromRight)
import Data.Text (isInfixOf)
import Effectful
import Effectful.Dispatch.Dynamic
import Network.Connection (TLSSettings (..))
import Network.HTTP.Conduit (httpLbs, mkManagerSettings, newManager, parseRequest, responseBody)
import Network.HTTP.Simple (setRequestHeader)
import Network.HTTP.Types.Header (hUserAgent)

import CommitSearch.Domain.Commit
import CommitSearch.Effects.DbCommit 

runDbCommitHttp :: IOE :> es => String -> Eff (DbCommit : es) a -> Eff es a
runDbCommitHttp url = interpret $ \_env -> \case

  AllCommits -> 
    liftIO $ query url

  CommitsFromTitle txt -> 
    filter (isInfixOf txt . title) <$> liftIO (query url)

query :: String -> IO [Commit]
query url = do
    manager <- newManager $ mkManagerSettings (TLSSettingsSimple True False False) Nothing
    request <- setRequestHeader hUserAgent ["commitsearch"] <$> parseRequest url
    fromRight [] . eitherDecode . responseBody <$> httpLbs request manager

-- https://docs.gitlab.com/ee/api/commits.html
ulcoGitlab :: String
ulcoGitlab = "https://gitlab.dpt-info.univ-littoral.fr/api/v4/projects/14/repository/commits"

