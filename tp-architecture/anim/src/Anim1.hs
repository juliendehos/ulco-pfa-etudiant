{-# LANGUAGE TemplateHaskell #-}

module Anim1 where

-- import Control.Lens
-- import Control.Monad
-- import Control.Monad.Trans.Reader
-- import Control.Monad.Trans.Writer
import Control.Monad.State.Strict
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game
import Linear.V2
import Linear.Vector
import System.Random
import System.Exit

-------------------------------------------------------------------------------
-- params
-------------------------------------------------------------------------------

winTitle :: String
winTitle = "Anim1"

winWidth, winHeight :: Int
winWidth = 400
winHeight = 300

winWidthF, winHeightF, xMin, xMax, yMin, yMax, ballRadius :: Float
winWidthF = fromIntegral winWidth
winHeightF = fromIntegral winHeight
xMin =  ballRadius - 0.5 * winWidthF
xMax = -ballRadius + 0.5 * winWidthF
yMin =  ballRadius - 0.5 * winHeightF
yMax = -ballRadius + 0.5 * winHeightF
ballRadius = 20

-------------------------------------------------------------------------------
-- types
-------------------------------------------------------------------------------

data Ball = Ball 
  { _pos :: V2 Float
  , _vel :: V2 Float
  } deriving (Show)

data Model = Model
  { _ball :: Ball
  , _nextBalls :: [Ball]
  }

instance Random Ball where
  randomR (Ball pos0 vel0, Ball pos1 vel1) = runState $
    Ball <$> state (randomR (pos0, pos1)) 
         <*> state (randomR (vel0, vel1))

  random = randomR ( Ball (V2 xMin yMin) (V2 (-500) (-500))
                   , Ball (V2 xMax yMax) (V2   500    500))

-------------------------------------------------------------------------------
-- update Ball
-------------------------------------------------------------------------------

updateMotion :: Float -> Ball -> Ball
updateMotion deltaTime ball0 =
  let (Ball p v) = ball0
  in ball0 { _pos = p + v ^* deltaTime }

updateBounces :: Ball -> Ball
updateBounces ball0 = 
  let
    (V2 px py) = _pos ball0
    (V2 vx vy) = _vel ball0
    ball1 = if px <= xMin
                then Ball (V2 (2*xMin - px) py) (V2 (-vx) vy)
                else ball0
    ball2 = if px >= xMax
                then Ball (V2 (2*xMax - px) py) (V2 (-vx) vy)
                else ball1
    ball3 = if py <= yMin 
                then Ball (V2 px (2*yMin - py)) (V2 vx (-vy))
                else ball2
    ball4 = if py >= yMax
                then Ball (V2 px (2*yMax - py)) (V2 vx (-vy))
                else ball3
  in ball4

-------------------------------------------------------------------------------
-- application
-------------------------------------------------------------------------------

run :: IO ()
run = do
  (ball0 : balls) <- randoms <$> getStdGen
  let model = Model ball0 balls
      window = InWindow winTitle (winWidth, winHeight) (0, 0)
      bgcolor = red
      fps = 30 
  playIO window bgcolor fps model handleDisplay handleEvent handleTime

handleDisplay :: Model -> IO Picture
handleDisplay model = 
  let (V2 x y) = _pos $ _ball model
  in pure $ Pictures
      [ translate x y (circleSolid ballRadius)
      , rectangleWire winWidthF winHeightF ]

handleEvent :: Event -> Model -> IO Model
handleEvent (EventKey (SpecialKey KeyEsc) Up _ _) _ = exitSuccess
handleEvent (EventKey (SpecialKey KeySpace) Up _ _) model = do
  let ball0 = head $ _nextBalls model 
  putStrLn $ "new ball: " ++ show ball0
  pure $ model { _ball = ball0
               , _nextBalls = tail (_nextBalls model) }
handleEvent _ model = pure model

handleTime :: Float -> Model -> IO Model
handleTime deltaTime model = do

  let app :: Ball -> Ball
      app = updateBounces . updateMotion deltaTime

  let b1 = app (_ball model)

  pure $ model { _ball = b1 }


