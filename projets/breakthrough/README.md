
# Breakthrough

Implémentation en Haskell du jeu
[Breakthrough](https://en.wikipedia.org/wiki/Breakthrough_(board_game)) (avec
interface texte et interface graphique).

## Planning

- [] newGame (sauf les coups possibles) + tests 
- [] affichage texte
- [] calcul des coups possibles + tests
- [] playMove + test
- [] interface texte
- [] interface graphique
- [] IA (random, monte-carlo...)

